import tensorflow as tf

message = tf.constant('Hello world')

with tf.Session() as sess:
  print(sess.run(message).decode())